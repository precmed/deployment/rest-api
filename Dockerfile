FROM python:3.6-alpine3.14

ENV KEYCLOAK_ACCESS_TRIES=5
ENV FLASK_WORKERS=1
ENV FLASK_THREADS=4

COPY requirements.txt /app/

WORKDIR /app

RUN pip install -r requirements.txt

RUN apk add --no-cache \
    yaml-dev \
    zip

COPY routines /app/routines
COPY config.json /app/
COPY app.py run_flask.sh /app/

RUN chmod 755 /app/run_flask.sh

EXPOSE 8000

CMD /app/run_flask.sh
