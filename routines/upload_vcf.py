from routines.routine import Routine
from flask import request
import logging
import os
import pymongo


class UploadVCF(Routine):

    def __init__(self, handle, user_id, groups):
        super().__init__(user_id, groups)
        self.handle = handle
        self.output_bucket_name = self.config["upload_vcf_bucket"]
        self.dir_path = "./storage/"
        self.local_inputs_path = self.dir_path + "inputs"
        if not os.path.exists(self.local_inputs_path):
            os.makedirs(self.local_inputs_path)

    def is_vcf_gzipped(self, vcffile):
        import re
        import gzip
        try:
            with open(vcffile, 'rb') as fp:
                buf = fp.read(80)
                zipped = False
                if (buf[0] == 0x1F and
                    buf[1] == 0x8B and
                        buf[2] == 0x8):
                    zipped = True
                    with gzip.open(vcffile, 'rb') as gf:
                        buf = gf.readline()
                line = buf.decode("utf-8")
                m = re.match(r"##fileformat=VCFv\d\.\d", line)
                return (m is not None, zipped)
        except Exception:
            return (False, False)

    def post(self):

        err = super().post()
        if err is not None:
            logging.error(err)
            return self.handle(err)

        file_key = self.orderId+".vcf.gz"
        with pymongo.MongoClient(os.getenv('MONGODB_ENDPOINT')+":"+os.getenv('MONGODB_PORT'),
                                 username=os.getenv('MONGODB_USER'),
                                 password=os.getenv('MONGODB_PASSWORD'),
                                 authSource=os.getenv('MONGODB_AUTHENTICATION_SOURCE')) as mongoclient:
            mongodb = mongoclient[os.getenv('MONGODB_DATABASE')]
            metadata_documents = mongodb["metadata"]
            query = {"orderId": self.orderId}
            workflows = metadata_documents.find(query)
            number_of_workflows = 0
            try:
                number_of_workflows = workflows.count()   
            except Exception as e:
                logging.error(e)
                return {"message": "Cannot connect to workflow metadata database."}      
           
            if number_of_workflows > 0:
                err = "Error: Referral Number already taken"
                logging.error(err)
                return self.handle(err)

        # also check if another file with the same name <orderid>.vcf.gz exists
        # since mongodb doesnt know about uploaded vcfs yet
        bucket = self.client.Bucket(self.output_bucket_name)
        try:
            for file in bucket.objects.all():
                if file_key == file.key:
                    err = "Error: Referral Number already taken"
                    logging.error(err)
                    return self.handle(err)
        except Exception as err:
            logging.error(err)
            return self.handle(err)

        if not request.files.get('file', None):
            err = "Error: Missing file"
            logging.error(err)
            return self.handle(err)
        file = request.files['file']
        local_filename = os.path.join(self.local_inputs_path, file.filename)
        
        real_local_inputs_path = os.path.realpath(self.local_inputs_path)
        real_local_filename = os.path.realpath(local_filename)
        
        if real_local_inputs_path != os.path.commonpath((real_local_inputs_path, real_local_filename)):
            err = "Error: This filename is not valid"
            logging.error(err)
            return self.handle(err)
        try:
            file.save(local_filename)
        except Exception as err:
            logging.error(err)
            return self.handle(err)

        is_vcf, is_gzipped = self.is_vcf_gzipped(local_filename)
        if not is_vcf:
            err = "Error: This file is not a valid VCF file"
            logging.error(err)
            return self.handle(err)
        if not is_gzipped:
            try:
                import gzip
                f_in = open(local_filename, 'rb')
                zipped_local_filename = local_filename+'.gz'
                f_out = gzip.GzipFile(zipped_local_filename, 'wb')
                f_out.write(f_in.read())
                f_out.close()
                f_in.close()
                os.remove(local_filename)
                local_filename = zipped_local_filename
            except Exception as err:
                logging.error(err)
                return self.handle(err)
        try:
            bucket.upload_file(Filename=local_filename, Key=file_key)
        except Exception as err:
            logging.error(err)
            return self.handle(err)

        try:
            os.remove(local_filename)
        except Exception as err:
            logging.error(err)
            return self.handle(err)

        return self.handle("Successfully uploaded")
