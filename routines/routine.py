import json
import logging
import os
import boto3
import requests
import uuid
from flask import request
from flask_restful import Resource
from routines.keycloak_admin import KeycloakAdmin

ka = KeycloakAdmin()
valid_groups = set(json.loads(os.environ.get("GROUPS")))


class Routine(Resource):

    def __init__(self, user_id, groups):
        with open("./config.json", "r") as fp:
            self.config = json.load(fp=fp)
        if groups is None:
            self.group = ka.get_group(user_id, valid_groups)
        else:
            self.group = ""
            for group_path in groups:
                group_path_list = group_path.split("/")
                if len(group_path_list) == 2 and group_path_list[0] == "pipelines" and group_path_list[1] in valid_groups:
                    self.group = group_path_list[1]
                    break

        self.cromwell = 'http://workflows-' + str(self.group) + "-cromwell:" + str(
            os.getenv('CROMWELL_PORT')) + "/api/workflows/v1"
        self.netloc = os.getenv('S3_GATEWAY')

        self.orderId = None
        self.output_bucket_name = self.config["outputs_bucket"]
        self.user_id = user_id
        if self.group:
            access_key, secret_key = self.get_access_keys()
            self.client = boto3.resource('s3',
                                         endpoint_url='http://' + self.netloc,
                                         aws_access_key_id=access_key,
                                         aws_secret_access_key=secret_key)

    def post(self):
        if 'type' in request.form and request.form['type'] == "custom":
            self.orderId = "custom_" + str(uuid.uuid4())
        elif 'orderId' not in request.form or request.form['orderId'] == "":
            err = "Error: Missing Referral Number or you did not choose custom workflow type."
            logging.error(err)
            return err
        else:
            self.orderId = request.form['orderId']

        if not self.group:
            err = "User does not belong to any group"
            logging.error(err)
            return err
        try:
            requests.get(self.cromwell)
        except Exception:
            err = "Cannot connect to " + self.cromwell
            logging.error(err)
            return err

        return None

    def get_access_keys(self):

        group_caps = self.group.upper().replace("-", "_")
        access_key = os.getenv(f'S3_{group_caps}_ACCESS_KEY')
        secret_key = os.getenv(f'S3_{group_caps}_SECRET_KEY')

        return access_key, secret_key
