from flask import request
from routines.routine import Routine
import pymongo
import os
import logging


class GetMetadata(Routine):
    def __init__(self, handle, user_id, groups):
        super().__init__(user_id, groups)
        self.handle = handle

    def post(self):
        # check mongo and get the document that corresponds to the order id in request and the user group.
        with pymongo.MongoClient(os.getenv('MONGODB_ENDPOINT')+":"+os.getenv('MONGODB_PORT'),
                                 username=os.getenv('MONGODB_USER'),
                                 password=os.getenv('MONGODB_PASSWORD'),
                                 authSource=os.getenv('MONGODB_AUTHENTICATION_SOURCE')) as mongoclient:
            mongodb = mongoclient[os.getenv('MONGODB_DATABASE')]
            metadata_documents = mongodb["metadata"]
            query = {"orderId": request.form['orderId']}
            workflows = metadata_documents.find(query)
            results = []

            number_of_workflows = 0
            try:
                number_of_workflows = workflows.count()   
            except Exception as e:
                logging.error(e)
                return {"message": "Cannot connect to workflow metadata database."}      
            
            if number_of_workflows == 0:
                return {"message": "No entries for your group with this order id"}
            for workflow in workflows:
                workflow['_id'] = str(workflow['_id'])
                results.append({'run' + str(workflow['run']): workflow})
            return {"results": results}
