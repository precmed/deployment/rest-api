import json
import logging
import os
import sys
from flask import Flask, g
from flask_oidc import OpenIDConnect
from flask_restful import Api
from routines.get_metadata import GetMetadata
from routines.get_order_ids import GetOrderIds
from routines.workflow import SubmitWorkflow
from routines.cancel import Cancel
from routines.list_workflows import ListWorkflows
from routines.upload_vcf import UploadVCF
from routines.status import Status
from routines.outputs import Outputs
import shutil

app = Flask(__name__)

app.config.update({
    'SECRET_KEY': os.environ.get('OIDC_SECRET'),
    'TESTING': True,
    'OIDC_CLIENT_SECRETS': os.environ.get('IODC_CLIENT_SECRETS_PATH'),
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_OPENID_REALM': os.environ.get('KEYCLOAK_REALM'),
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token'
})

oidc = OpenIDConnect(app)
api = Api(app)


@app.route('/api', methods=['GET'])
@oidc.accept_token(require_token=False)
def hello_api():
    if not hasattr(g, 'oidc_token_info'):
        return json.dumps({'hello': 'Welcome anonymous'})
    else:
        return json.dumps({'hello': 'Welcome %s' % g.oidc_token_info['sub']})


@app.route('/launch', methods=['POST'])
@oidc.accept_token(require_token=True)
def launch_post():
    mygroups = None
    if 'mygroups' in g.oidc_token_info:
        mygroups = g.oidc_token_info['mygroups']
    submit_workflow = SubmitWorkflow(
        lambda x: x, g.oidc_token_info['sub'], g.oidc_token_info['username'], mygroups)
    r = submit_workflow.post()
    return r


@app.route('/metadata', methods=['POST'])
@oidc.accept_token(require_token=True)
def metadata_post():
    mygroups = None
    if 'mygroups' in g.oidc_token_info:
        mygroups = g.oidc_token_info['mygroups']
    get_metadata = GetMetadata(
        lambda x: x, g.oidc_token_info['sub'], mygroups)
    r = get_metadata.post()
    return r


@app.route('/runs', methods=['GET'])
@oidc.accept_token(require_token=True)
def orderids_get():
    mygroups = None
    if 'mygroups' in g.oidc_token_info:
        mygroups = g.oidc_token_info['mygroups']
    get_order_ids = GetOrderIds(
        lambda x: x, g.oidc_token_info['sub'], mygroups)
    r = get_order_ids.post()
    return r


@app.route('/cancel', methods=['POST'])
@oidc.accept_token(require_token=True)
def cancel_post():
    mygroups = None
    if 'mygroups' in g.oidc_token_info:
        mygroups = g.oidc_token_info['mygroups']
    cancel = Cancel(
        lambda x: x, g.oidc_token_info['sub'], mygroups)
    return cancel.post()


@app.route('/list_workflows', methods=['GET'])
@oidc.accept_token(require_token=True)
def list_workflow_get():
    workflows = ListWorkflows()
    return workflows.get()


@app.route('/upload_vcf', methods=['POST'])
@oidc.accept_token(require_token=True)
def upload_vcf_post():
    mygroups = None
    if 'mygroups' in g.oidc_token_info:
        mygroups = g.oidc_token_info['mygroups']
    upload = UploadVCF(
        lambda x: x, g.oidc_token_info['sub'], mygroups)
    return upload.post()


@app.route('/status', methods=['POST'])
@oidc.accept_token(require_token=True)
def status_post():
    mygroups = None
    if 'mygroups' in g.oidc_token_info:
        mygroups = g.oidc_token_info['mygroups']
    status = Status(
        lambda x: x, g.oidc_token_info['sub'], mygroups)
    r = status.post()
    return r


@app.route('/outputs', methods=['POST'])
@oidc.accept_token(require_token=True)
def outputs_post():
    mygroups = None
    if 'mygroups' in g.oidc_token_info:
        mygroups = g.oidc_token_info['mygroups']
    outputs = Outputs(
        lambda x: x, g.oidc_token_info['sub'], mygroups)
    directory, zipfile = outputs.post()

    def generate():
        with open(zipfile, "rb") as f:
            while True:
                data = f.read(1024)
                if not data:
                    break
                yield data
        try:
            os.remove(zipfile)
            shutil.rmtree(directory)
        except Exception as err:
            logging.error(err)

    r = app.response_class(generate(), mimetype='application/zip')
    r.headers.set('Content-Disposition', 'attachment', filename='outputs.zip')
    return r


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

if __name__ == '__main__':
    app.run(debug=True)
